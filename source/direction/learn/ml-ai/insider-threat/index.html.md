---
layout: markdown_page
title: "Category Direction - Insider Threat"
description: "Insider Threat identifies attacks and high risk behaviors by correlating different data sources and observing behavioral patterns."
canonical_path: "/direction/learn/ml-ai/insider-threat/"
---

## Description
Insider Threat is a way to identify attacks and high risk behaviors by correlating different data sources and observing behavioral patterns. This allows attacks to be observed that are only apparent with context from multiple sources, rather than just a single event in isolation.

### Goal


### Roadmap
Forthcoming

## What's Next & Why


## Top Vision Item(s)

[Item 1](https://gitlab.com/groups/gitlab-org/-/epics/2545)
